// swift-tools-version:5.7.1
import PackageDescription

let package = Package(
    name: "identomat",
    defaultLocalization: "en",
    platforms: [.iOS(.v12)],
    products: [
        .library(
            name: "identomat",
            targets: ["identomatSPM"]),
    ],
    dependencies: [
        .package(url: "https://github.com/stasel/WebRTC.git", from: "106.0.0")
    ],
    targets: [
        .binaryTarget(
            name: "identomat",
            path: "identomat.xcframework.zip"),
        .target(
            name: "identomatSPM",
            dependencies: ["identomat", "WebRTC"]
        )
    ])
